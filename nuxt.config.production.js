export default {
    env: {
      VUE_APP_BASE_URL: 'https://appex.ai',
      VUE_APP_DEBUG: false,
  },
  // Target (https://go.nuxtjs.dev/config-target)
  target: 'static',
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'Applied Experience',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'In jedem Bauprojekt kosten vorhersehbare Überraschungen Nerven, Zeit und Geld. Die Software AppEx hilft mit gezieltem Einsatz von gesammeltem Wissen, teure Blindflüge zu vermeiden.' },
      {
          property: 'og:title',
          content: `Applied Experience`,
      },
      {
          property: 'og:type',
          content: `website`,
      },
      {
          property: 'og:description',
          content: `Applied Experience`,
      },
      {
          property: 'og:image',
          content: '@/assets/img/logo.png',
      },

      { name: 'msapplication-TileColor', content: '#da532c' },
      { name: 'theme-color', content: '#da532c' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [
      { rel: 'manifest', href: 'manifest.json' },
      { rel: 'icon', type: 'image/png', href: 'favicon-32x32.png' },
      { rel: 'apple-touch-icon', sizes: '180x180', href: '/favicon/apple-touch-icon.png' },
    ],
    script: [
      {
        type: 'application/ld+json',

      }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#da532c' },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
    // Import SCSS
    '~assets/scss/style.scss',
    '~assets/scss/spacing.scss',
  ],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    { src: '~~/node_modules/vue-rellax/lib/nuxt-plugin', ssr: false },
    '~/plugins/jsonld'
  ],

  components: true,

  buildModules: [
    '@nuxt/typescript-build',
    '@nuxtjs/pwa',
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    'nuxt-buefy',
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
  ],

  pwa: {
    manifest: {
      name: 'Applied Experience',
      short_name: 'AppEx',
      lang: 'en',
      display: 'standalone',
      background_color: '#fff',
      description: 'Applied Experience',
      useWebmanifestExtension: false,
      theme_color: '#fff'
    },
    icon: {
      fileName: 'icon.png'
    }
},

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {},

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
  }
}
